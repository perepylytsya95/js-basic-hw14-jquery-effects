$(document).ready(function (){
    $(document).on('click', '.header--search_image', function (){
        $('.header--page_nav-wrapper').toggle( "slow", () => {
        });
    })
    $(window).scroll(function (){
        if(window.pageYOffset > window.innerHeight){
            $('.scroll-to-top-btn').addClass('active');
        } else{
            $('.scroll-to-top-btn').removeClass('active');
        }
    })
    $(document).on('click', '.hide-show-btn', function (){
        $('.section3').slideToggle('slow', () => {});
    })
});

const  scrollToTopBtn = document.createElement('button');
scrollToTopBtn.classList.add('scroll-to-top-btn');
scrollToTopBtn.innerText = "Go Top";
document.body.lastElementChild.previousElementSibling.before(scrollToTopBtn);

scrollToTopBtn.addEventListener('click', function (){
    window.scrollTo({top: 0, behavior: 'smooth'})
});

const hideShow = document.createElement('button');
hideShow.classList.add('hide-show-btn');
hideShow.innerText = "Hide/Show Top Rated"
document.querySelector('.section3').after(hideShow);



